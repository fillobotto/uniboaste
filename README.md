# UniboAste #

Il progetto UniboAste permette agli utenti della rete Unibo di scambiare oggetti tra loro attraverso un sistema di aste online.

L’applicazione dovrà permettere ad un utente di:

* Registrarsi al sito
* Mettere in vendita un oggetto
* Fare un’offerta per un oggetto messo in vendita
* Chiedere informazioni al venditore dell’oggetto

### Registrazione al sito ###

Durante la registrazione al sito l’utente dovrà inserire obbligatoriamente le seguenti informazioni personali:

* Nome utente (username)
* Nome e Cognome
* Numero di telefono
* Password
* Indirizzo email
* Codice Fiscale
* Indirizzo di domicilio

Inoltre l’utente potrà opzionalmente fornire i seguenti dati:

* Sesso
* Data di nascita
* Luogo di nascita

Una volta registrato l’utente non potrà più modificare i propri dati.
Non è previsto un meccanismo di verifica dell’indirizzo mail, né di recupero password.

### Visualizzare pagine profilo ###

Gli utenti registrati possono accedere ad una propria pagina profilo che riporta i dati con i quali è avvenuta la registrazione; gli oggetti per i quali è stata effettuata una offerta (con indicazione dello stato: asta chiusa e oggetto aggiudicato, asta chiusa e oggetto non aggiudicato, asta in corso e offerta migliore, asta in corso e offerta superata) e gli oggetti posti in vendita (con indicazione dello stato: asta in corso, asta conclusa).

### Mettere in vendita un oggetto ###

Un utente registrato potrà mettere in vendita un oggetto, specificando la descrizione, il prezzo base di vendita, la data di scadenza dell’asta e la categoria.
Solo l’amministratore potrà eliminare l’oggetto messo in vendita.

### Gestione Categorie ###

Al lancio del sistema, il sito web dovrà offrire alcune categorie predefinite di oggetti: Abbigliamento, Casa, Elettronica, Giardinaggio, Sport. Durante l’esercizio del sistema, l’amministratore potrà aggiungere nuove categorie, rinominare una categoria esistente o aggiungere una sotto categoria ad una categoria esistente. Una sotto categoria può avere a sua volta sotto categorie: es. Sport > Calcio > Scarpe da Calcio.

### Fare un’offerta per un oggetto messo in vendita ###

Un utente registrato potrà fare un’offerta per un oggetto messo in vendita, specificando dalla pagina di descrizione dell’oggetto l’importo offerto. Solo l’amministratore potrà eliminare l’offerta.
Alla scadenza dell’asta sulla pagina di descrizione dell’oggetto verrà visualizzato l’username dell’utente che ha fatto l’offerta maggiore e l’importo dell’offerta che ha vinto l’asta.

### Fare una domanda per un oggetto messo in vendita ###

Un utente registrato potrà inviare una domanda per chiedere informazioni riguardo un oggetto messo in vendita.
Alla domanda potrà rispondere solo il venditore dell’oggetto.
L’amministratore potrà eliminare domande e risposte.

### Aste Concluse ###

Alla conclusione di un’asta l’utente che ha offerto l’importo maggiore si aggiudica l’oggetto. Dopo la scadenza dell’asta, se c’è un vincitore, il sistema mostrerà nella pagina di descrizione dell’oggetto messo in vendita: il nome dell’utente che si è aggiudicato l’asta e l’importo con il quale si è aggiudicato l’oggetto. Non sono richiesti ulteriori sistemi di notifica. Una volta conclusa l’asta gli oggetti non appaiono più nella lista di quelli messi in vendita ma sono accessibili solo dalle pagine di profilo del venditore e dei partecipanti all’asta.

### Visualizzazione oggetti messi in vendita ###

Un visitatore potrà vedere i nomi degli oggetti messi in vendita nella pagina principale del sito web.
Gli oggetti messi in vendita dovranno essere mostrati in ordine cronologico dall’oggetto con scadenza più vicina.
Cliccando sul nome degli oggetti i visitatori verranno indirizzati su una nuova pagina in cui verrà mostrata (nel caso in cui non sia stata raggiunta la data di scadenza dell’oggetto): la descrizione dell’oggetto, l’attuale offerta più alta, le domande e le risposte relative all’oggetto fatte al venditore.
Gli utenti registrati, oltre alla visualizzazione degli oggetti come visitatori troveranno nella pagina principale del sito un menu a tendina (o una colonna laterale) dal quale potranno selezionare una categoria. Una volta selezionata una categoria verranno visualizzate solo gli oggetti appartenenti a quella categoria e a sue eventuali sotto categorie. Una volta scelta una categoria verranno mostrate all’utente eventuali sottocategorie di quella scelta. L’utente potrà cliccare nuovamente su una sottocategoria per raffinare ulteriormente la lista di oggetti visualizzati.

### Nota bene ###

* Tutte le informazioni gestite dal sito dovranno essere accessibili anche dopo il riavvio del server. Per gestire la persistenza è obbligatorio utilizzare MapDB (MapDB). Non si accetterà la consegna di progetti che usano altri meccanismi di gestione della persistenza.
* L’utente amministratore dovrà avere come username “admin” e password “admin”. La sua registrazione non potrà avvenire attraverso interfaccia web.