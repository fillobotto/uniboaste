package com.uniboaste.client;

import static org.junit.Assert.*;

import java.util.ArrayList;
import java.util.Date;

import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import com.google.gwt.core.client.GWT;
import com.google.gwt.junit.client.GWTTestCase;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.rpc.ServiceDefTarget;
import com.uniboaste.client.services.AuthenticationService;
import com.uniboaste.client.services.AuthenticationServiceAsync;
import com.uniboaste.client.services.DataService;
import com.uniboaste.client.services.DataServiceAsync;
import com.uniboaste.shared.Product;
import com.uniboaste.shared.User;

public class ProductTesting extends GWTTestCase  {

	public void testAddProduct() {
		DataServiceAsync dataService = GWT.create(DataService.class);
		ServiceDefTarget target = (ServiceDefTarget) dataService;
	      target.setServiceEntryPoint(GWT.getModuleBaseURL() 
	      + "data");
		delayTestFinish(1000);
		dataService.addProduct(new Product("none", "none", 100.0d, new Date(), "Casa"), new AsyncCallback<Void>() {
			
			@Override
			public void onSuccess(Void result) {
				dataService.getProductsByCategory("Casa", new AsyncCallback<ArrayList<Product>>() {
					
					@Override
					public void onSuccess(ArrayList<Product> result) {
						assertTrue(result != null && result.size() > 0);
						finishTest();
					}
					
					@Override
					public void onFailure(Throwable caught) {
						fail();
						finishTest();
					}
				});
			}
			
			@Override
			public void onFailure(Throwable caught) {
				fail();
				finishTest();
			}
		});
		
	}
	
	@Override
	public String getModuleName() {
		return "com.uniboaste.UniboAste";
	}

}
