package com.uniboaste.client;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import com.google.gwt.core.client.GWT;
import com.google.gwt.junit.client.GWTTestCase;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.rpc.ServiceDefTarget;
import com.uniboaste.client.services.AuthenticationService;
import com.uniboaste.client.services.AuthenticationServiceAsync;
import com.uniboaste.shared.User;

public class AccountTesting extends GWTTestCase  {

	public void testLoginoginInvalid() {
		AuthenticationServiceAsync authService = GWT.create(AuthenticationService.class);
		ServiceDefTarget target = (ServiceDefTarget) authService;
	      target.setServiceEntryPoint(GWT.getModuleBaseURL() 
	      + "auth");
		delayTestFinish(1000);
		authService.login("none0", "none0", new AsyncCallback<User>() {
			
			@Override
			public void onSuccess(User result) {
				assertFalse(result != null);
				finishTest();
			}
			
			@Override
			public void onFailure(Throwable caught) {
				fail(caught.getMessage());
				finishTest();
			}
		});
		
	}

	public void testAdminLogin() {
		AuthenticationServiceAsync authService = GWT.create(AuthenticationService.class);
		ServiceDefTarget target = (ServiceDefTarget) authService;
	      target.setServiceEntryPoint(GWT.getModuleBaseURL() 
	      + "auth");
		delayTestFinish(1000);
		authService.login("admin", "admin", new AsyncCallback<User>() {
			
			@Override
			public void onSuccess(User result) {
				assertTrue(result != null);
				finishTest();
			}
			
			@Override
			public void onFailure(Throwable caught) {
				fail(caught.getMessage());
				finishTest();
			}
		});
		
	}
	
	public void testRegistration() {
		final AuthenticationServiceAsync authService = GWT.create(AuthenticationService.class);
		ServiceDefTarget target = (ServiceDefTarget) authService;
	      target.setServiceEntryPoint(GWT.getModuleBaseURL() 
	      + "auth");
		delayTestFinish(3000);
		
		authService.register("none1", "none1", "none1", "000", "none1", "none1", "none", "none", "none", "none", "none", new AsyncCallback<Boolean>() {
			
			@Override
			public void onSuccess(Boolean result) {
				authService.login("none1", "none1", new AsyncCallback<User>() {
					
					@Override
					public void onSuccess(User result) {
						assertTrue(result != null);
						finishTest();
					}
					
					@Override
					public void onFailure(Throwable caught) {
						fail(caught.getMessage());
						finishTest();
					}
				});
			}
			
			@Override
			public void onFailure(Throwable caught) {
				fail("Failed registration");
				finishTest();
			}
		});
	}
	
	@Override
	public String getModuleName() {
		return "com.uniboaste.UniboAste";
	}

}
