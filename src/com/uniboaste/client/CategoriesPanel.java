/*
 * Copyright 2007 Google Inc.
 * 
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License. You may obtain a copy of
 * the License at
 * 
 * http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations under
 * the License.
 */
package com.uniboaste.client;

import java.util.ArrayList;
import java.util.List;

import com.google.gwt.core.client.GWT;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.logical.shared.SelectionEvent;
import com.google.gwt.event.logical.shared.SelectionHandler;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.uibinder.client.UiField;
import com.google.gwt.uibinder.client.UiHandler;
import com.google.gwt.user.cellview.client.CellList;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.Composite;
import com.google.gwt.user.client.ui.SimplePanel;
import com.google.gwt.user.client.ui.TextBox;
import com.google.gwt.user.client.ui.Tree;
import com.google.gwt.user.client.ui.TreeItem;
import com.google.gwt.user.client.ui.VerticalPanel;
import com.google.gwt.user.client.ui.Widget;
import com.google.gwt.view.client.ListDataProvider;
import com.uniboaste.client.services.AuthenticationService;
import com.uniboaste.client.services.AuthenticationServiceAsync;
import com.uniboaste.client.services.DataService;
import com.uniboaste.client.services.DataServiceAsync;
import com.uniboaste.shared.Category;
import com.uniboaste.shared.HomeCallback;
import com.uniboaste.shared.Product;
import com.uniboaste.shared.ProductCell;
import com.uniboaste.shared.ProductItemCallback;
import com.uniboaste.shared.User;
import com.uniboaste.shared.UserType;

/**
 * The top panel, which contains the 'welcome' message and various links.
 */
public class CategoriesPanel extends Composite implements ProductItemCallback {

	interface Binder extends UiBinder<Widget, CategoriesPanel> {
	}

	private static final Binder binder = GWT.create(Binder.class);
	
	private final DataServiceAsync dataService = GWT.create(DataService.class);
	private final AuthenticationServiceAsync authService = GWT.create(AuthenticationService.class);
	
	HomeCallback callback;
	
	@UiField
	Tree categoriesList;
	@UiField
	TextBox txtRenameCat;
	@UiField
	TextBox txtAddCat;
	
	@UiField
	SimplePanel centerPanel;
	@UiField
	VerticalPanel renamePanel;
	@UiField
	VerticalPanel addPanel;
	
	ListDataProvider<Product> dataProvider;
	
	public CategoriesPanel(HomeCallback callback, Category category) {
		initWidget(binder.createAndBindUi(this));
		
		this.callback = callback;
		
		TreeItem root = new TreeItem();
		root.setText("Categories");

		for(Category c : category.getChildren()) {
			TreeItem t0 = new TreeItem();
			t0.setText(c.getName());
			root.addItem(t0);
			root.setState(true);
			setC1(t0, c);
		}
		
		categoriesList.addItem(root);
		
		CellList<Product> table = new CellList<Product>(new ProductCell(this));
		dataProvider = new ListDataProvider<Product>();
		dataProvider.addDataDisplay(table);
		centerPanel.add(table);
		
		categoriesList.addSelectionHandler(new SelectionHandler<TreeItem>() {
			
			@Override
			public void onSelection(SelectionEvent<TreeItem> event) {
				txtRenameCat.setText(event.getSelectedItem().getText());
				dataService.getProductsByCategory(event.getSelectedItem().getText(), new AsyncCallback<ArrayList<Product>>() {
					
					@Override
					public void onSuccess(ArrayList<Product> result) {
						List<Product> list = dataProvider.getList();
						list.clear();
						for(Product p : result)
							list.add(p);
						dataProvider.refresh();
					}
					
					@Override
					public void onFailure(Throwable caught) {
					}
				});
			}
		});
		
		dataService.getProductsByCategory("Categories", new AsyncCallback<ArrayList<Product>>() {
			
			@Override
			public void onSuccess(ArrayList<Product> result) {
				List<Product> list = dataProvider.getList();
				list.clear();
				for(Product p : result)
					list.add(p);
				dataProvider.refresh();
			}
			
			@Override
			public void onFailure(Throwable caught) {
			}
		});
		
		authService.isAuthenticated(new AsyncCallback<User>() {
			
			@Override
			public void onSuccess(User result) {
				if(result != null && result.getUserType() == UserType.Administrator) {
					addPanel.setVisible(true);
					renamePanel.setVisible(true);
				}
			}
			
			@Override
			public void onFailure(Throwable caught) {
				// TODO Auto-generated method stub
				
			}
		});
	}
	
	void setC1(TreeItem root, Category category) {
		for(Category c : category.getChildren()) {
			setC(root, c);
		}
	}
	
	void setC(TreeItem root, Category category) {
		TreeItem t0 = new TreeItem();
		t0.setText(category.getName());
		root.addItem(t0);

		for(Category c : category.getChildren()) {
			setC(root.getChild(root.getChildIndex(t0)), c);
		}
	}

	/***
	 * Apertura prodotto selezionato
	 */
	@Override
	public void clicked(Product product) {
		callback.openProduct(product);
	}
	
	/***
	 * Invia ridenominazione categoria
	 * @param event
	 */
	@UiHandler("renameButton")
	void onRenameButtonClick(ClickEvent event) {
		if(categoriesList.getSelectedItem() == null) return;
		String current = categoriesList.getSelectedItem().getText();
		if(current.equals("Categories")) return;
		if(txtRenameCat.getText().length() == 0) return;
		
		dataService.renameCategory(current, txtRenameCat.getText(), new AsyncCallback<Void>() {

			@Override
			public void onFailure(Throwable caught) {			
			}

			@Override
			public void onSuccess(Void result) {
				dataService.getCategoryTree(new AsyncCallback<Category>() {
					
					@Override
					public void onSuccess(Category result) {
						categoriesList.clear();
						
						TreeItem root = new TreeItem();
						root.setText("Categories");

						for(Category c : result.getChildren()) {
							TreeItem t0 = new TreeItem();
							t0.setText(c.getName());
							root.addItem(t0);
							root.setState(true);
							setC1(t0, c);
						}
						
						categoriesList.addItem(root);
					}
					
					@Override
					public void onFailure(Throwable caught) {
					}
				});
			}
		});
	}
	
	/***
	 * Invia ggiunta categoria
	 * @param event
	 */
	@UiHandler("addButton")
	void onAddButtonClick(ClickEvent event) {
		if(categoriesList.getSelectedItem() == null) return;
		String current = categoriesList.getSelectedItem().getText();
		if(txtAddCat.getText().length() == 0) return;
		
		dataService.addCategory(current, txtAddCat.getText(), new AsyncCallback<Void>() {

			@Override
			public void onFailure(Throwable caught) {			
			}

			@Override
			public void onSuccess(Void result) {
				dataService.getCategoryTree(new AsyncCallback<Category>() {
					
					@Override
					public void onSuccess(Category result) {
						categoriesList.clear();
						
						TreeItem root = new TreeItem();
						root.setText("Categories");

						for(Category c : result.getChildren()) {
							TreeItem t0 = new TreeItem();
							t0.setText(c.getName());
							root.addItem(t0);
							root.setState(true);
							setC1(t0, c);
						}
						
						categoriesList.addItem(root);
					}
					
					@Override
					public void onFailure(Throwable caught) {
					}
				});
			}
		});
	}
}
