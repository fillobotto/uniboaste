/*
 * Copyright 2007 Google Inc.
 * 
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License. You may obtain a copy of
 * the License at
 * 
 * http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations under
 * the License.
 */
package com.uniboaste.client;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import com.google.gwt.core.client.GWT;
import com.google.gwt.dom.client.HeadingElement;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.i18n.shared.DateTimeFormat;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.uibinder.client.UiField;
import com.google.gwt.uibinder.client.UiHandler;
import com.google.gwt.user.cellview.client.CellList;
import com.google.gwt.user.client.DOM;
import com.google.gwt.user.client.Window;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.Button;
import com.google.gwt.user.client.ui.Composite;
import com.google.gwt.user.client.ui.Label;
import com.google.gwt.user.client.ui.SimplePanel;
import com.google.gwt.user.client.ui.TextBox;
import com.google.gwt.user.client.ui.Widget;
import com.google.gwt.view.client.ListDataProvider;
import com.uniboaste.client.dialogs.OfferDialog;
import com.uniboaste.client.dialogs.QuestionDialog;
import com.uniboaste.client.dialogs.RegistrationDialog;
import com.uniboaste.client.services.AuthenticationService;
import com.uniboaste.client.services.AuthenticationServiceAsync;
import com.uniboaste.client.services.DataService;
import com.uniboaste.client.services.DataServiceAsync;
import com.uniboaste.shared.Bid;
import com.uniboaste.shared.OfferCallback;
import com.uniboaste.shared.Product;
import com.uniboaste.shared.ProductOwnCell;
import com.uniboaste.shared.Question;
import com.uniboaste.shared.QuestionCallback;
import com.uniboaste.shared.QuestionCell;
import com.uniboaste.shared.QuestionItemCallback;
import com.uniboaste.shared.User;
import com.uniboaste.shared.UserType;

/**
 * The top panel, which contains the 'welcome' message and various links.
 */
public class AuctionPanel extends Composite implements OfferCallback, QuestionItemCallback, QuestionCallback {

	private final DataServiceAsync dataService = GWT.create(DataService.class);
	private final AuthenticationServiceAsync authService = GWT.create(AuthenticationService.class);
	
	interface Binder extends UiBinder<Widget, AuctionPanel> {
	}

	private static final Binder binder = GWT.create(Binder.class);
	
	private Product product;
	
	@UiField
	Label descriptionLabel;
	@UiField
	Label baseLabel;
	@UiField
	Label expirationLabel;
	@UiField
	Label timeLabel;
	@UiField
	Button addBid;
	@UiField
	Button deleteProd;
	@UiField
	Button deleteBid;
	@UiField
	Button addQuestion;
	
	@UiField
	SimplePanel questionsPanel;
	ListDataProvider<Question> dataProvider;
	
	public AuctionPanel(Product product) {
		this.product = product;
		initWidget(binder.createAndBindUi(this));
	}
	
	@Override
	protected
	void onLoad()
	{
		HeadingElement  divID = (HeadingElement) DOM.getElementById("title").cast();
		divID.setInnerText(product.getName());	
		descriptionLabel.setText("Description: " + product.getDescription());
		final String prize = String.valueOf(product.getPrize());
		final Label temp = baseLabel;
		dataService.getBids(product.getId(), new AsyncCallback<ArrayList<Bid>>() {
			
			@Override
			public void onSuccess(ArrayList<Bid> result) {
				if(result.size() == 0) {
					if(product.isOpen())
						temp.setText("Starting offer: " + prize);
					else
						temp.setText("Product not sold");
				} else {
					if(product.isOpen())
						temp.setText("Highest bid: " + result.get(0).getAmount() + " made by " + result.get(0).getAuthor());
					else
						temp.setText("Product sold for: " + result.get(0).getAmount() + " to " + result.get(0).getAuthor());
				}
			}
			
			@Override
			public void onFailure(Throwable caught) {	
			}
		});
		final Button temp0 = deleteProd;
		final Button temp1 = addBid;
		final Button temp2 = deleteBid;
		final Button temp3 = addQuestion;
		authService.isAuthenticated(new AsyncCallback<User>() {

			@Override
			public void onFailure(Throwable caught) {
			}

			@Override
			public void onSuccess(final User result0) {
				if(result0 != null && result0.getUserType().equals(UserType.Administrator)) {
					temp0.setVisible(true);
					if(product.isOpen()) temp2.setVisible(true);
				} if(result0 != null && product.isOpen()) {
					temp1.setVisible(true);
					temp3.setVisible(true);
				}
				
				if(product.getAuthor().equals(result0.getUsername())) {
					temp1.setVisible(false); temp3.setVisible(false);
				}
				
				dataService.getQuestions(product.getId(), new AsyncCallback<ArrayList<Question>>() {
					
					@Override
					public void onSuccess(ArrayList<Question> result) {
						CellList<Question> table = new CellList<Question>(new QuestionCell(AuctionPanel.this, product, result0 != null && result0.getUsername().equals(product.getAuthor()),  result0 != null && result0.getUserType() == UserType.Administrator));
						dataProvider = new ListDataProvider<Question>();
						dataProvider.addDataDisplay(table);
						dataProvider.getList().addAll(result);
						questionsPanel.add(table);
					}
					
					@Override
					public void onFailure(Throwable caught) {
					}
				});
			}
		});
		DateTimeFormat fm = DateTimeFormat.getFormat("dd/MM/yyyy HH:mm:ss");
		expirationLabel.setText("Expiration: " + fm.format(new Date(product.getExpirationDate())));
	}
	
	/***
	 * Mostra dialog aggiunta offerta
	 * @param event
	 */
	@UiHandler("addBid")
	void onAddBidClick(ClickEvent event) {
		OfferDialog dialog = new OfferDialog(this, product);
		dialog.center();
		dialog.show();
	}
	
	/***
	 * Mostra dialog aggiunta domanda
	 * @param event
	 */
	@UiHandler("addQuestion")
	void onAddQuestionClick(ClickEvent event) {
		QuestionDialog dialog = new QuestionDialog(this, product);
		dialog.center();
		dialog.show();
	}
	
	/***
	 * Rimuove l'ultima offerta
	 * @param event
	 */
	@UiHandler("deleteBid")
	void onDeleteBidClick(ClickEvent event) {
		dataService.deleteLastBid(product.getId(), new AsyncCallback<Void>() {

			@Override
			public void onFailure(Throwable caught) {
			}

			@Override
			public void onSuccess(Void result) {
				final Label temp = baseLabel;
				dataService.getBids(product.getId(), new AsyncCallback<ArrayList<Bid>>() {
					
					@Override
					public void onSuccess(ArrayList<Bid> result) {
						if(result.size() == 0) {
							temp.setText("Starting offer: " + product.getPrize());
						} else {
							temp.setText("Highest bid: " + result.get(0).getAmount());
						}
					}
					
					@Override
					public void onFailure(Throwable caught) {
					}
				});
			}
		});
	}
	
	/***
	 * Elimina il prodotto corrente
	 * @param event
	 */
	@UiHandler("deleteProd")
	void onDeleteClick(ClickEvent event) {
		dataService.deleteProduct(product.getId(), new AsyncCallback<Void>() {

			@Override
			public void onFailure(Throwable caught) {
			}

			@Override
			public void onSuccess(Void result) {
				Window.alert("Product deleted correctly");
			}
		});
	}

	/***
	 * Callback per aggiungere l'offerta
	 */
	@Override
	public void offerDone(double amount) {
		dataService.addBid(product.getId(), amount, new AsyncCallback<Void>() {

			@Override
			public void onFailure(Throwable caught) {
				
			}

			@Override
			public void onSuccess(Void result) {
				final Label temp = baseLabel;
				dataService.getBids(product.getId(), new AsyncCallback<ArrayList<Bid>>() {
					
					@Override
					public void onSuccess(ArrayList<Bid> result) {
						if(result.size() == 0) {
							temp.setText("Starting offer: " + product.getPrize());
						} else {
							temp.setText("Highest bid: " + result.get(0).getAmount());
						}
					}
					
					@Override
					public void onFailure(Throwable caught) {
					}
				});
			}
		});
	}

	/***
	 * Callback per aggiungere risposta alla domanda
	 */
	@Override
	public void answer(String pid, String qid, String reply) {
		dataService.answerQuestion(pid, qid, reply, new AsyncCallback<Void>() {
			
			@Override
			public void onSuccess(Void result) {
				dataService.getQuestions(product.getId(), new AsyncCallback<ArrayList<Question>>() {
					
					@Override
					public void onSuccess(ArrayList<Question> result) {
						List<Question> list = dataProvider.getList();
						list.clear();
						for(Question p : result)
							list.add(p);
						dataProvider.refresh();
					}
					
					@Override
					public void onFailure(Throwable caught) {
					}
				});
			}
			
			@Override
			public void onFailure(Throwable caught) {
			}
		});
	}

	/***
	 * Callback per aggiungere domanda
	 */
	@Override
	public void postQuestion(String pid, String title) {
		dataService.addQuestion(pid, title, new AsyncCallback<Void>() {
			
			@Override
			public void onSuccess(Void result) {
				dataService.getQuestions(product.getId(), new AsyncCallback<ArrayList<Question>>() {
					
					@Override
					public void onSuccess(ArrayList<Question> result) {
						List<Question> list = dataProvider.getList();
						list.clear();
						for(Question p : result)
							list.add(p);
						dataProvider.refresh();
					}
					
					@Override
					public void onFailure(Throwable caught) {

					}
				});
			}
			
			@Override
			public void onFailure(Throwable caught) {

			}
		});
	}

	/***
	 * Callback per rimuovere domanda
	 */
	@Override
	public void delete(String pid, String qid) {
		dataService.deleteQuestion(pid, qid, new AsyncCallback<Void>() {
			
			@Override
			public void onSuccess(Void result) {
				dataService.getQuestions(product.getId(), new AsyncCallback<ArrayList<Question>>() {
					
					@Override
					public void onSuccess(ArrayList<Question> result) {
						List<Question> list = dataProvider.getList();
						list.clear();
						for(Question p : result)
							list.add(p);
						dataProvider.refresh();
					}
					
					@Override
					public void onFailure(Throwable caught) {

					}
				});
			}
			
			@Override
			public void onFailure(Throwable caught) {
			}
		});
	}
}
