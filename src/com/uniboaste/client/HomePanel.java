/*
 * Copyright 2007 Google Inc.
 * 
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License. You may obtain a copy of
 * the License at
 * 
 * http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations under
 * the License.
 */
package com.uniboaste.client;

import java.util.ArrayList;
import java.util.List;

import com.google.gwt.core.client.GWT;
import com.google.gwt.event.logical.shared.SelectionEvent;
import com.google.gwt.event.logical.shared.SelectionHandler;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.uibinder.client.UiField;
import com.google.gwt.user.cellview.client.CellList;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.Composite;
import com.google.gwt.user.client.ui.SimplePanel;
import com.google.gwt.user.client.ui.Tree;
import com.google.gwt.user.client.ui.TreeItem;
import com.google.gwt.user.client.ui.Widget;
import com.google.gwt.view.client.ListDataProvider;
import com.uniboaste.client.services.DataService;
import com.uniboaste.client.services.DataServiceAsync;
import com.uniboaste.shared.Category;
import com.uniboaste.shared.HomeCallback;
import com.uniboaste.shared.Product;
import com.uniboaste.shared.ProductCell;
import com.uniboaste.shared.ProductItemCallback;

/**
 * The top panel, which contains the 'welcome' message and various links.
 */
public class HomePanel extends Composite implements ProductItemCallback {

	interface Binder extends UiBinder<Widget, HomePanel> {
	}

	private static final Binder binder = GWT.create(Binder.class);
	
	private final DataServiceAsync dataService = GWT.create(DataService.class);
	
	HomeCallback callback;
	
	@UiField
	SimplePanel centerPanel;
	
	ListDataProvider<Product> dataProvider;
	
	public HomePanel(HomeCallback callback) {
		initWidget(binder.createAndBindUi(this));
		
		this.callback = callback;
		
		CellList<Product> table = new CellList<Product>(new ProductCell(this));
		dataProvider = new ListDataProvider<Product>();
		dataProvider.addDataDisplay(table);
		centerPanel.add(table);
		
		dataService.getOpenProducts(new AsyncCallback<ArrayList<Product>>() {
			
			@Override
			public void onSuccess(ArrayList<Product> result) {
				List<Product> list = dataProvider.getList();
				list.clear();
				for(Product p : result)
					list.add(p);
				dataProvider.refresh();
			}
			
			@Override
			public void onFailure(Throwable caught) {
				// TODO Auto-generated method stub
				
			}
		});
	}

	/***
	 * Callback aprtura pagina prodotto
	 */
	@Override
	public void clicked(Product product) {
		callback.openProduct(product);
	}
}
