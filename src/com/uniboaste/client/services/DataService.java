package com.uniboaste.client.services;

import java.util.ArrayList;

import com.google.gwt.user.client.rpc.RemoteService;
import com.google.gwt.user.client.rpc.RemoteServiceRelativePath;
import com.uniboaste.shared.Bid;
import com.uniboaste.shared.Category;
import com.uniboaste.shared.Product;
import com.uniboaste.shared.Question;

import jdk.nashorn.internal.runtime.arrays.ArrayLikeIterator;

/**
 * The client-side stub for the RPC service.
 * AuthenticationService manages all calls related to account management.
 */
@RemoteServiceRelativePath("data.rpc")
public interface DataService extends RemoteService {
	public Category getCategoryTree();
	
	public void addProduct(Product product);
	
	public ArrayList<Product> getProductsByCategory(String name);
	
	public ArrayList<Product> getOwnProducts();
	
	public ArrayList<Bid> getOwnBids();
	
	public void addBid(String pid, double amount);
	
	public ArrayList<Bid> getBids(String pid);
	
	public void deleteProduct(String pid);

	ArrayList<Question> getQuestions(String pid);

	void addQuestion(String pid, String title);

	void answerQuestion(String pid, String qid, String reply);

	ArrayList<Product> getOpenProducts();

	void deleteQuestion(String pid, String qid);

	void deleteLastBid(String id);

	void renameCategory(String query, String to);

	void addCategory(String query, String to);
}
