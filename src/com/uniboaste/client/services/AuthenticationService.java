package com.uniboaste.client.services;

import com.google.gwt.user.client.rpc.RemoteService;
import com.google.gwt.user.client.rpc.RemoteServiceRelativePath;
import com.uniboaste.shared.User;

/**
 * The client-side stub for the RPC service.
 * AuthenticationService manages all calls related to account management.
 */
@RemoteServiceRelativePath("auth.rpc")
public interface AuthenticationService extends RemoteService {
	
	Boolean register(String username, String name, String surname, String mobile, String password, String email, String address, String cf, String gender, String birth, String birthplace);
	
	User login(String username, String password);
	
	User isAuthenticated();

	void logout();
}
