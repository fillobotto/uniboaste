package com.uniboaste.client.services;

import java.util.ArrayList;

import com.google.gwt.user.client.rpc.AsyncCallback;
import com.uniboaste.shared.Bid;
import com.uniboaste.shared.Category;
import com.uniboaste.shared.Product;
import com.uniboaste.shared.Question;

/**
 * The async counterpart of <code>AuthenticationService</code>.
 */
public interface DataServiceAsync {
	public void getCategoryTree(AsyncCallback<Category> callback);
	
	public void addProduct(Product product, AsyncCallback<Void> v);
	
	public void getProductsByCategory(String name, AsyncCallback<ArrayList<Product>> p);
	
	public void addBid(String pid, double amount, AsyncCallback<Void> v);
	
	public void getBids(String pid, AsyncCallback<ArrayList<Bid>> bids);
	
	public void deleteProduct(String pid, AsyncCallback<Void> v);

	public void getOwnProducts(AsyncCallback<ArrayList<Product>> asyncCallback);
	
	public void getOwnBids(AsyncCallback<ArrayList<Bid>> asyncCallback);

	void getQuestions(String pid, AsyncCallback<ArrayList<Question>> callback);

	void addQuestion(String pid, String title, AsyncCallback<Void> callback);

	void answerQuestion(String pid, String qid, String reply, AsyncCallback<Void> callback);

	void getOpenProducts(AsyncCallback<ArrayList<Product>> callback);

	void deleteQuestion(String pid, String qid, AsyncCallback<Void> callback);

	public void deleteLastBid(String id, AsyncCallback<Void> asyncCallback);

	void renameCategory(String query, String to, AsyncCallback<Void> callback);

	void addCategory(String query, String to, AsyncCallback<Void> callback);
}
