package com.uniboaste.client.services;

import com.google.gwt.user.client.rpc.AsyncCallback;
import com.uniboaste.shared.User;

/**
 * The async counterpart of <code>AuthenticationService</code>.
 */
public interface AuthenticationServiceAsync {
	void login(String username, String password, AsyncCallback<User> callback);
	void register(String username, String name, String surname, String mobile, String password, String email, String address, String cf, String gender, String birth, String birthplace, AsyncCallback<Boolean> callback);
	void isAuthenticated(AsyncCallback<User> callback);
	void logout(@SuppressWarnings("rawtypes") AsyncCallback callback);
}
