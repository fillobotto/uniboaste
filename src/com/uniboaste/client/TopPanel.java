/*
 * Copyright 2007 Google Inc.
 * 
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License. You may obtain a copy of
 * the License at
 * 
 * http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations under
 * the License.
 */
package com.uniboaste.client;

import com.google.gwt.core.client.GWT;
import com.google.gwt.dom.client.Style;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.uibinder.client.UiField;
import com.google.gwt.uibinder.client.UiHandler;
import com.google.gwt.user.client.DOM;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.Anchor;
import com.google.gwt.user.client.ui.Composite;
import com.google.gwt.user.client.ui.HTML;
import com.google.gwt.user.client.ui.Widget;
import com.uniboaste.client.dialogs.LoginDialog;
import com.uniboaste.client.dialogs.RegistrationDialog;
import com.uniboaste.client.services.AuthenticationService;
import com.uniboaste.client.services.AuthenticationServiceAsync;
import com.uniboaste.shared.HomeCallback;
import com.uniboaste.shared.LoginCallback;
import com.uniboaste.shared.User;

/**
 * The top panel, which contains the 'welcome' message and various links.
 */
public class TopPanel extends Composite implements LoginCallback {

	interface Binder extends UiBinder<Widget, TopPanel> {
	}

	private static final Binder binder = GWT.create(Binder.class);

	private final AuthenticationServiceAsync loginService = GWT.create(AuthenticationService.class);

	@UiField
	Anchor signLink;
	@UiField
	Anchor registerLink;
	@UiField
	Anchor profileLink;
	@UiField
	Anchor categoriesLink;
	@UiField
	Anchor addLink;
	@UiField
	Anchor homeLink;
	
	private HomeCallback callback;
	
	public TopPanel() {
		initWidget(binder.createAndBindUi(this));

		loginService.isAuthenticated(new AsyncCallback<User>() {

			@Override
			public void onSuccess(User result) {
				handleAuthentication(result);
			}

			@Override
			public void onFailure(Throwable caught) {

			}
		});
	}

	public void setParentPanel(HomeCallback callback) {
		this.callback = callback;
	}
	
	/***
	 * Sistema l'intrfacia dopo login/out
	 * @param user
	 */
	void handleAuthentication(User user) {
		if (user != null) {
			signLink.setText("Sign out");
			registerLink.setVisible(false);
			DOM.getElementById("welcomeCaption").removeAllChildren();
			DOM.getElementById("welcomeCaption")
					.appendChild(new HTML("<p>Welcome back, " + user.getUsername() + "</p>").getElement());
			DOM.getElementById("welcomeCaption").getStyle().setDisplay(Style.Display.INLINE_BLOCK);
			profileLink.setVisible(true);
			addLink.setVisible(true);
			categoriesLink.setVisible(true);
			callback.loggedIn(user);
		}
	}

	/***
	 * Bottone di login/out
	 * @param event
	 */
	@UiHandler("signLink")
	void onSignClicked(ClickEvent event) {
		if(signLink.getText().equals("Sign out")) {
			loginService.logout(new AsyncCallback<Object>() {

				@Override
				public void onFailure(Throwable caught) {
					// TODO Auto-generated method stub
					
				}

				@Override
				public void onSuccess(Object result) {
					// TODO Auto-generated method stub
					
				}
			});
			signLink.setText("Sign in");
			registerLink.setVisible(true);
			profileLink.setVisible(false);
			addLink.setVisible(false);
			categoriesLink.setVisible(false);
			DOM.getElementById("welcomeCaption").getStyle().setDisplay(Style.Display.NONE);
			callback.loggedOut();
		} else {
			LoginDialog dialog = new LoginDialog(this);
			dialog.center();
			dialog.show();
		}	
	}

	/***
	 * Bottone di registrazione
	 * @param event
	 */
	@UiHandler("registerLink")
	void onRegisterClicked(ClickEvent event) {
		RegistrationDialog dialog = new RegistrationDialog();
		dialog.center();
		dialog.show();
	}

	/***
	 * Bottone per andare al profilo
	 * @param event
	 */
	@UiHandler("profileLink")
	void onProfileClicked(ClickEvent event) {
		loginService.isAuthenticated(new AsyncCallback<User>() {

			@Override
			public void onSuccess(User result) {
				callback.openProfile(result);
			}

			@Override
			public void onFailure(Throwable caught) {

			}
		});
	}

	/***
	 * Bottone per aprire le categorie
	 * @param event
	 */
	@UiHandler("categoriesLink")
	void onCategoriesClicked(ClickEvent event) {
		callback.openCategories();
	}

	/***
	 * Bottone per aggiungere un prodotto
	 * @param event
	 */
	@UiHandler("addLink")
	void onAddClicked(ClickEvent event) {
		callback.addProduct();
	}
	
	/***
	 * Bottone per tornare alla home
	 * @param event
	 */
	@UiHandler("homeLink")
	void onHomeClicked(ClickEvent event) {
		callback.goHome();
	}
	
	@Override
	public void loggedIn(User user) {
		handleAuthentication(user);
	}
}
