/*
 * Copyright 2007 Google Inc.
 * 
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License. You may obtain a copy of
 * the License at
 * 
 * http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations under
 * the License.
 */
package com.uniboaste.client.dialogs;

import com.google.gwt.core.client.GWT;
import com.google.gwt.dom.client.NativeEvent;
import com.google.gwt.dom.client.Style;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.KeyCodes;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.uibinder.client.UiField;
import com.google.gwt.uibinder.client.UiHandler;
import com.google.gwt.user.client.Event.NativePreviewEvent;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.Button;
import com.google.gwt.user.client.ui.DialogBox;
import com.google.gwt.user.client.ui.HTML;
import com.google.gwt.user.client.ui.HTMLPanel;
import com.google.gwt.user.client.ui.PasswordTextBox;
import com.google.gwt.user.client.ui.TextBox;
import com.google.gwt.user.client.ui.Widget;
import com.uniboaste.client.services.AuthenticationService;
import com.uniboaste.client.services.AuthenticationServiceAsync;

/**
 * A simple example of an 'about' dialog box.
 */
public class RegistrationDialog extends DialogBox {

	private final AuthenticationServiceAsync loginService = GWT.create(AuthenticationService.class);

	interface Binder extends UiBinder<Widget, RegistrationDialog> {
	}

	private static final Binder binder = GWT.create(Binder.class);

	@UiField
	Button closeButton;
	@UiField
	Button registerButton;

	@UiField
	TextBox txtUsername;
	@UiField
	TextBox txtName;
	@UiField
	TextBox txtSurname;
	@UiField
	TextBox txtPhone;
	@UiField
	PasswordTextBox txtPassword;
	@UiField
	TextBox txtEmail;
	@UiField
	TextBox txtCF;
	@UiField
	TextBox txtAddress;
	@UiField
	TextBox txtGender;
	@UiField
	TextBox txtBirth;
	@UiField
	TextBox txtBirthplace;

	@UiField
	HTMLPanel errorPanel;

	public RegistrationDialog() {
		setText("Registration form");
		Widget widget = binder.createAndBindUi(this);

		setWidget(widget);

		setAnimationEnabled(true);
		setGlassEnabled(true);
	}

	@Override
	protected void onPreviewNativeEvent(NativePreviewEvent preview) {
		super.onPreviewNativeEvent(preview);

		NativeEvent evt = preview.getNativeEvent();
		if (evt.getType().equals("keydown")) {
			switch (evt.getKeyCode()) {
			case KeyCodes.KEY_ENTER:
			case KeyCodes.KEY_ESCAPE:
				hide();
				break;
			}
		}
	}

	/***
	 * Close the registration form
	 * 
	 * @param event
	 */
	@UiHandler("closeButton")
	void onSignOutClicked(ClickEvent event) {
		hide();
	}

	/***
	 * Register a new user
	 * 
	 * @param event
	 */
	@UiHandler("registerButton")
	void onRegisterClicked(ClickEvent event) {
		errorPanel.clear();
		errorPanel.getElement().getStyle().setDisplay(Style.Display.NONE);

		if (txtUsername.getText().length() == 0) {
			errorPanel.add(new HTML("<p>Username field cannot be empty</p>"));
			errorPanel.getElement().getStyle().setDisplay(Style.Display.BLOCK);
			return;
		}

		if (txtName.getText().length() == 0) {
			errorPanel.add(new HTML("<p>Name field cannot be empty</p>"));
			errorPanel.getElement().getStyle().setDisplay(Style.Display.BLOCK);
			return;
		}

		if (txtSurname.getText().length() == 0) {
			errorPanel.add(new HTML("<p>Surname field cannot be empty</p>"));
			errorPanel.getElement().getStyle().setDisplay(Style.Display.BLOCK);
			return;
		}

		if (txtPhone.getText().length() == 0) {
			errorPanel.add(new HTML("<p>Phone field cannot be empty</p>"));
			errorPanel.getElement().getStyle().setDisplay(Style.Display.BLOCK);
			return;
		}

		if (txtPassword.getText().length() == 0) {
			errorPanel.add(new HTML("<p>Password field cannot be empty</p>"));
			errorPanel.getElement().getStyle().setDisplay(Style.Display.BLOCK);
			return;
		}

		if (txtEmail.getText().length() == 0) {
			errorPanel.add(new HTML("<p>Email field cannot be empty</p>"));
			errorPanel.getElement().getStyle().setDisplay(Style.Display.BLOCK);
			return;
		}

		if (txtCF.getText().length() == 0) {
			errorPanel.add(new HTML("<p>CF field cannot be empty</p>"));
			errorPanel.getElement().getStyle().setDisplay(Style.Display.BLOCK);
			return;
		}

		if (txtAddress.getText().length() == 0) {
			errorPanel.add(new HTML("<p>Address field cannot be empty</p>"));
			errorPanel.getElement().getStyle().setDisplay(Style.Display.BLOCK);
			return;
		}

		loginService.register(txtUsername.getText(), txtName.getText(), txtSurname.getText(), txtPhone.getText(),
				txtPassword.getText(), txtEmail.getText(), txtAddress.getText(), txtCF.getText(), txtGender.getText(),
				txtBirth.getText(), txtBirthplace.getText(), new AsyncCallback<Boolean>() {

					@Override
					public void onSuccess(Boolean result) {
						if (result)
							hide();
						else {
							errorPanel.clear();
							errorPanel.add(new HTML("<p>Username already in use</p>"));
							errorPanel.getElement().getStyle().setDisplay(Style.Display.BLOCK);
						}
					}

					@Override
					public void onFailure(Throwable caught) {
						// TODO Auto-generated method stub
					}
				});

		hide();
	}
}
