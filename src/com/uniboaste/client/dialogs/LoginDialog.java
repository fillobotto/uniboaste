/*
 * Copyright 2007 Google Inc.
 * 
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License. You may obtain a copy of
 * the License at
 * 
 * http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations under
 * the License.
 */
package com.uniboaste.client.dialogs;

import com.google.gwt.core.client.GWT;
import com.google.gwt.dom.client.NativeEvent;
import com.google.gwt.dom.client.Style;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.KeyCodes;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.uibinder.client.UiField;
import com.google.gwt.uibinder.client.UiHandler;
import com.google.gwt.user.client.Event.NativePreviewEvent;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.Button;
import com.google.gwt.user.client.ui.DialogBox;
import com.google.gwt.user.client.ui.HTML;
import com.google.gwt.user.client.ui.HTMLPanel;
import com.google.gwt.user.client.ui.PasswordTextBox;
import com.google.gwt.user.client.ui.TextBox;
import com.google.gwt.user.client.ui.Widget;
import com.uniboaste.client.services.AuthenticationService;
import com.uniboaste.client.services.AuthenticationServiceAsync;
import com.uniboaste.shared.LoginCallback;
import com.uniboaste.shared.User;

/**
 * A simple example of an 'about' dialog box.
 */
public class LoginDialog extends DialogBox {

	private final AuthenticationServiceAsync loginService = GWT.create(AuthenticationService.class);
	
	interface Binder extends UiBinder<Widget, LoginDialog> { }
	private static final Binder binder = GWT.create(Binder.class);

	@UiField Button closeButton;
	@UiField Button loginButton;
	
	@UiField TextBox txtUsername;
	@UiField PasswordTextBox txtPassword;
	
	@UiField HTMLPanel errorPanel;
	
	LoginCallback callback;
	
	public LoginDialog(LoginCallback callback) {
		this.callback = callback;
		
		setText("Login form");
		setWidget(binder.createAndBindUi(this));

		setAnimationEnabled(true);
		setGlassEnabled(true);
	}

	@Override
	protected void onPreviewNativeEvent(NativePreviewEvent preview) {
		super.onPreviewNativeEvent(preview);

	    NativeEvent evt = preview.getNativeEvent();
	    if (evt.getType().equals("keydown")) {
	    	switch (evt.getKeyCode()) {
	    		case KeyCodes.KEY_ENTER:
    			case KeyCodes.KEY_ESCAPE:
    				hide();
					break;
    		}
	    }
	}
	
	/***
	 * Close the login form
	 * @param event
	 */
	@UiHandler("closeButton")
	void onSignOutClicked(ClickEvent event) {
		hide();
	}
	
	/***
	 * Attempt login
	 * @param event
	 */
	@UiHandler("loginButton")
	void onLoginClicked(ClickEvent event) {
		errorPanel.clear();
		errorPanel.getElement().getStyle().setDisplay(Style.Display.NONE);
		
		if(txtUsername.getText().length() == 0) {
			errorPanel.add(new HTML("<p>Type the username</p>"));
			errorPanel.getElement().getStyle().setDisplay(Style.Display.BLOCK);
			return;
		}
		
		if(txtPassword.getText().length() == 0) {
			errorPanel.add(new HTML("<p>Type the password</p>"));
			errorPanel.getElement().getStyle().setDisplay(Style.Display.BLOCK);
			return;
		}
		
		loginService.login(txtUsername.getText(), txtPassword.getText(), new AsyncCallback<User>() {
			
			@Override
			public void onSuccess(User result) {
				if(result != null) {
					hide();
					callback.loggedIn(result);
				}
			}
			
			@Override
			public void onFailure(Throwable caught) {
				
			}
		});
	}
}
