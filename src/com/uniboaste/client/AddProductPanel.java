/*
 * Copyright 2007 Google Inc.
 * 
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License. You may obtain a copy of
 * the License at
 * 
 * http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations under
 * the License.
 */
package com.uniboaste.client;


import java.util.Date;


import com.google.gwt.core.client.GWT;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.uibinder.client.UiField;
import com.google.gwt.uibinder.client.UiHandler;
import com.google.gwt.user.client.Window;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.Button;
import com.google.gwt.user.client.ui.Composite;
import com.google.gwt.user.client.ui.TextBox;
import com.google.gwt.user.client.ui.Tree;
import com.google.gwt.user.client.ui.TreeItem;
import com.google.gwt.user.client.ui.Widget;
import com.google.gwt.user.datepicker.client.DateBox;
import com.google.gwt.user.datepicker.client.DatePicker;
import com.uniboaste.client.services.DataService;
import com.uniboaste.client.services.DataServiceAsync;
import com.uniboaste.shared.Category;
import com.uniboaste.shared.Product;

public class AddProductPanel extends Composite {

	interface Binder extends UiBinder<Widget, AddProductPanel> {
	}

	private static final Binder binder = GWT.create(Binder.class);
	
	@UiField
	DateBox datePicker;
	@UiField
	Button addButton;
	@UiField
	TextBox txtTitle;
	@UiField
	TextBox txtDescr;
	@UiField
	TextBox txtBase;
	@UiField
	Tree categoriesList;
	
	private final DataServiceAsync dataService = GWT.create(DataService.class);
	
	public AddProductPanel() {
		initWidget(binder.createAndBindUi(this));
		
		dataService.getCategoryTree(new AsyncCallback<Category>() {
			
			@Override
			public void onSuccess(Category result) {
				TreeItem root = new TreeItem();
				root.setText("Categories");
				
				for(Category c : result.getChildren()) {
					TreeItem t0 = new TreeItem();
					t0.setText(c.getName());
					root.addItem(t0);
					root.setState(true);
					setC1(t0, c);
				}
				
				categoriesList.addItem(root);
			}
			
			@Override
			public void onFailure(Throwable caught) {
			}
		});
	}
	

	/***
	 * Invia richiesta aggiunta prodotto
	 * @param event
	 */
	@UiHandler("addButton")
	void onAddButtonClick(ClickEvent event) {
		Date expDate = datePicker.getValue();
		String name = txtTitle.getText();
		String descr = txtDescr.getText();
		double prize = Double.valueOf(txtBase.getText());
		String cat = categoriesList.getSelectedItem().getText();
		if(cat.equals("Categories")) {
			Window.alert("Select a category");
			return;
		}
		Product prod = new Product(name, descr, prize, expDate, cat);
		dataService.addProduct(prod, new AsyncCallback<Void>() {

			@Override
			public void onFailure(Throwable caught) {
				
			}

			@Override
			public void onSuccess(Void result) {
				Window.alert("Prodotto inserito con successo");
			}
		});
	}
	
	void setC1(TreeItem root, Category category) {
		for(Category c : category.getChildren()) {
			setC(root, c);
		}
	}
	
	void setC(TreeItem root, Category category) {
		TreeItem t0 = new TreeItem();
		t0.setText(category.getName());
		root.addItem(t0);

		for(Category c : category.getChildren()) {
			setC(root.getChild(root.getChildIndex(t0)), c);
		}
	}
}
