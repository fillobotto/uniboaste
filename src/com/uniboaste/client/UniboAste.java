package com.uniboaste.client;

import com.google.gwt.core.client.EntryPoint;
import com.google.gwt.core.client.GWT;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.uibinder.client.UiField;
import com.google.gwt.user.client.Window;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.DockLayoutPanel;
import com.google.gwt.user.client.ui.RootLayoutPanel;
import com.uniboaste.client.services.DataService;
import com.uniboaste.client.services.DataServiceAsync;
import com.uniboaste.shared.Category;
import com.uniboaste.shared.HomeCallback;
import com.uniboaste.shared.Product;
import com.uniboaste.shared.ProfilePageCallback;
import com.uniboaste.shared.User;

/**
 * Entry point classes define <code>onModuleLoad()</code>.
 */
public class UniboAste implements EntryPoint, HomeCallback, ProfilePageCallback {

	interface Binder extends UiBinder<DockLayoutPanel, UniboAste> { }
	private final DataServiceAsync dataService = GWT.create(DataService.class);
	private static final Binder binder = GWT.create(Binder.class);
	
	@UiField
	TopPanel topPanel;
	@UiField
	MainPanel mainPanel;
	DockLayoutPanel dock;
	
	/**
	 * This is the entry point method.
	 */
	public void onModuleLoad() {
		dock = binder.createAndBindUi(this);

	    Window.enableScrolling(false);
	    Window.setMargin("0px");
	    
	    topPanel.setParentPanel(this);
	    //dock.setWidgetHidden(dock.getWidget(1), true);
	    RootLayoutPanel root = RootLayoutPanel.get();
	    root.add(dock);	
	    mainPanel.add(new HomePanel(this));
	}

	/***
	 * Called callback whenever the user login. Makes the main container visible
	 * and opens categories view.
	 */
	@Override
	public void loggedIn(User user) {
		//dock.setWidgetHidden(dock.getWidget(1), false);	
		openCategories();
	}

	/***
	 * Called callback whenever the user logout. Makes the main container invisible
	 * since it's not possible to use the platform without being signed in.
	 */
	@Override
	public void loggedOut() {
		mainPanel.add(new HomePanel(this));
	}

	/***
	 * Callback called when clicking Profile item in top menu bar.
	 */
	@Override
	public void openProfile(User user) {
		mainPanel.add(new ProfilePanel(this, user));
	}

	/***
	 * Callback called when clicking Categories item in top menu bar.
	 */
	@Override
	public void openCategories() {
		dataService.getCategoryTree(new AsyncCallback<Category>() {
			
			@Override
			public void onSuccess(Category result) {
				mainPanel.add(new CategoriesPanel(UniboAste.this, result));
			}
			
			@Override
			public void onFailure(Throwable caught) {
			}
		});
	}

	/***
	 * Callback called when clicking Add Product item in top menu bar.
	 */
	@Override
	public void addProduct() {
		mainPanel.add(new AddProductPanel());
	}
	
	/***
	 * callback called when the user clicks on a Product item
	 */
	@Override
	public void openProduct(Product product) {
		mainPanel.add(new AuctionPanel(product));
	}

	/***
	 * Callback to return to the home page
	 */
	@Override
	public void goHome() {
		mainPanel.add(new HomePanel(this));	
	}
}
