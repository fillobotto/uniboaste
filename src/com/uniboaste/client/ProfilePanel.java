/*
 * Copyright 2007 Google Inc.
 * 
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License. You may obtain a copy of
 * the License at
 * 
 * http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations under
 * the License.
 */
package com.uniboaste.client;

import java.util.ArrayList;
import java.util.List;

import com.google.gwt.core.client.GWT;
import com.google.gwt.dom.client.HeadingElement;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.uibinder.client.UiField;
import com.google.gwt.user.cellview.client.CellList;
import com.google.gwt.user.client.DOM;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.Composite;
import com.google.gwt.user.client.ui.Label;
import com.google.gwt.user.client.ui.SimplePanel;
import com.google.gwt.user.client.ui.Widget;
import com.google.gwt.view.client.ListDataProvider;
import com.uniboaste.client.services.DataService;
import com.uniboaste.client.services.DataServiceAsync;
import com.uniboaste.shared.Bid;
import com.uniboaste.shared.BidCell;
import com.uniboaste.shared.Product;
import com.uniboaste.shared.ProductCell;
import com.uniboaste.shared.ProductItemCallback;
import com.uniboaste.shared.ProductOwnCell;
import com.uniboaste.shared.ProfilePageCallback;
import com.uniboaste.shared.User;

/**
 * The top panel, which contains the 'welcome' message and various links.
 */
public class ProfilePanel extends Composite implements ProductItemCallback {

	interface Binder extends UiBinder<Widget, ProfilePanel> {
	}

	private static final Binder binder = GWT.create(Binder.class);
	
	private User user;
	private ProfilePageCallback callback;
	
	public ProfilePanel(ProfilePageCallback callback, User user) {
		this.user = user;
		this.callback = callback;
		initWidget(binder.createAndBindUi(this));
	}
	
	@UiField Label labelName;
	@UiField Label labelSurname;
	@UiField Label labelPhone;
	@UiField Label labelPassword;
	@UiField Label labelEmail;
	@UiField Label labelCF;
	@UiField Label labelAddress;
	@UiField Label labelGender;
	@UiField Label labelBirth;
	@UiField Label labelBirthplace;
	
	@UiField
	SimplePanel firstPanel;
	ListDataProvider<Product> dataProvider;
	
	@UiField
	SimplePanel secondPanel;
	ListDataProvider<Bid> dataBidProvider;
	
	private final DataServiceAsync dataService = GWT.create(DataService.class);
	
	@Override
	protected
	void onLoad()
	{
		HeadingElement  divID = (HeadingElement) DOM.getElementById("title").cast();
		divID.setInnerText("Profile of " + user.getUsername());	
		labelName.setText("Name: " + user.getName());
		labelSurname.setText("Surname: " + user.getSurname());
		labelPhone.setText("Phone number: " + user.getPhone());
		labelPassword.setText("Password: " + user.getPassword());
		labelEmail.setText("Email address: " + user.getEmail());
		labelCF.setText("CF: " + user.getCf());
		labelAddress.setText("Address: " + user.getAddress());
		labelGender.setText("Gender: " + user.getGender());
		labelBirth.setText("Birth: " + user.getBirth());
		labelBirthplace.setText("Birthplace: " + user.getBirthplace());
		
		CellList<Product> table = new CellList<Product>(new ProductOwnCell(this));
		dataProvider = new ListDataProvider<Product>();
		dataProvider.addDataDisplay(table);
		firstPanel.add(table);
		
		CellList<Bid> table2 = new CellList<Bid>(new BidCell(this));
		dataBidProvider = new ListDataProvider<Bid>();
		dataBidProvider.addDataDisplay(table2);
		secondPanel.add(table2);
		
		dataService.getOwnProducts(new AsyncCallback<ArrayList<Product>>() {
			
			@Override
			public void onSuccess(ArrayList<Product> result) {
				List<Product> list = dataProvider.getList();
				list.clear();
				for(Product p : result)
					list.add(p);
				dataProvider.refresh();
			}
			
			@Override
			public void onFailure(Throwable caught) {
				// TODO Auto-generated method stub
				
			}
		});
		
		dataService.getOwnBids(new AsyncCallback<ArrayList<Bid>>() {
			
			@Override
			public void onSuccess(ArrayList<Bid> result) {
				List<Bid> list = dataBidProvider.getList();
				list.clear();
				for(Bid p : result)
					list.add(p);
				dataBidProvider.refresh();
			}
			
			@Override
			public void onFailure(Throwable caught) {
				// TODO Auto-generated method stub
				
			}
		});
	}

	@Override
	public void clicked(Product product) {
		callback.openProduct(product);
	}
}
