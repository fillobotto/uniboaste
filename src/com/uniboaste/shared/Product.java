package com.uniboaste.shared;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;

import com.google.gwt.user.client.rpc.IsSerializable;

@SuppressWarnings("serial")
public class Product implements IsSerializable, Serializable {
	
	private String id;
	private String name;
	private String description;
	private double prize;
	private long expirationDate;
	private String category;
	private String author;
	private ArrayList<Bid> bids;
	private ArrayList<Question> questions;
	
	
	public ArrayList<Question> getQuestions() {
		if(questions == null) questions = new ArrayList<>();
		return questions;
	}

	public void setQuestions(ArrayList<Question> questions) {
		this.questions = questions;
	}

	public ArrayList<Bid> getBids() {
		if(bids == null) bids = new ArrayList<>();
		return bids;
	}
	
	public void setBids(ArrayList<Bid> bids) {
		this.bids = bids;
	}
	
	public boolean isOpen() {
		return open;
	}

	public void setOpen(boolean open) {
		this.open = open;
	}

	private boolean open;
	
	public Product() {}
	
	public Product(String name,String description,double prize,Date expirationDate,String category) {
		this.name = name;
		this.description = description;
		this.prize = prize;
		this.expirationDate = expirationDate.getTime();
		this.category = category;
	}
	
	
	
	
	public String getId() {
		return id;
	}

	public void setId(String string) {
		this.id = string;
	}

	public void setAuthor(String author) {
		this.author = author;
	}
	
	public String getAuthor() {
		return this.author;
	}
	
	
	
	
	public void setName(String name) {
		this.name = name;
	}
	
	public String getName() {
		return this.name;
	}
	
	
	
	
	public void setDescription(String description) {
		this.description = description;
	}
	
	public String getDescription() {
		return this.description;
	}
	
	
	
	
	public void setPrize(double prize) {
		this.prize = prize;
	}
	
	public double getPrize() {
		return this.prize;
	}
	
	
	public void setExpirationDate(long expirationDate) {
		this.expirationDate = expirationDate;
	}
	
	public long getExpirationDate() {
		return this.expirationDate;
	}
	
	public void setCategory(String category) {
		this.category = category;
	}
	
	public String getCategory() {
		return this.category;
	}
	
	
}
