package com.uniboaste.shared;

public interface HomeCallback {
	
	void loggedIn(User user);
	
	void loggedOut();
	
	void openProfile(User user);
	
	void openCategories();
	
	void addProduct();
	
	void openProduct(Product product);

	void goHome();
}
