package com.uniboaste.shared;

import com.google.gwt.cell.client.AbstractCell;
import com.google.gwt.cell.client.ValueUpdater;
import com.google.gwt.core.client.GWT;
import com.google.gwt.dom.client.BrowserEvents;
import com.google.gwt.dom.client.Element;
import com.google.gwt.dom.client.NativeEvent;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.MouseOverEvent;
import com.google.gwt.safehtml.shared.SafeHtmlBuilder;
import com.google.gwt.uibinder.client.UiHandler;
import com.google.gwt.uibinder.client.UiRenderer;

public class BidCell extends AbstractCell<Bid> {
	
	static interface Renderer extends UiRenderer {
		void render(SafeHtmlBuilder sb, String title, String status);
		void onBrowserEvent(BidCell o, NativeEvent e, Element p, Bid value);
	}

	  private final Renderer renderer;
	  private final ProductItemCallback callback;
	  
	  public BidCell(ProductItemCallback _callback) {
	    super(BrowserEvents.CLICK, BrowserEvents.MOUSEOVER);
	 	renderer = GWT.create(Renderer.class);
	 	callback = _callback;
	  }

	  @Override
	  public void render(Context context, final Bid item, final SafeHtmlBuilder sb) {
	  		String status;
	  		if(!item.isClosed()) 
	  			status = "Not yet closed";
	  		else
	  			if(item.isHigest()) {
	  				status = "Auction closed - You win it!";
	  			} else {
	  				status = "Auction closed - You lost it";
	  			}
	    renderer.render(sb, item.getProductName(), status);
	  }

	  @Override
	  public void onBrowserEvent(Context context, Element parent,
			  Bid value, NativeEvent event, ValueUpdater<Bid> valueUpdater) {
	    renderer.onBrowserEvent(this, event, parent, value);
	  }

	}