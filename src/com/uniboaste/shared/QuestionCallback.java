package com.uniboaste.shared;

public interface QuestionCallback {
	void postQuestion(String pid, String title);
}
