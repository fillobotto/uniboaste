package com.uniboaste.shared;

import com.google.gwt.cell.client.AbstractCell;
import com.google.gwt.cell.client.ValueUpdater;
import com.google.gwt.core.client.GWT;
import com.google.gwt.dom.client.BrowserEvents;
import com.google.gwt.dom.client.Element;
import com.google.gwt.dom.client.EventTarget;
import com.google.gwt.dom.client.NativeEvent;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.MouseOverEvent;
import com.google.gwt.safehtml.shared.SafeHtmlBuilder;
import com.google.gwt.uibinder.client.UiField;
import com.google.gwt.uibinder.client.UiHandler;
import com.google.gwt.uibinder.client.UiRenderer;
import com.google.gwt.user.client.Window;
import com.google.gwt.user.client.ui.Button;
import com.uniboaste.client.dialogs.AnswerDialog;
import com.uniboaste.client.dialogs.QuestionDialog;

public class QuestionCell extends AbstractCell<Question> {
	
	static interface Renderer extends UiRenderer {
		void render(SafeHtmlBuilder sb, String title, String reply, String visbutton, String visreply, String visdelete);
		void onBrowserEvent(QuestionCell o, NativeEvent e, Element p, Question value);
	}

	  private final Renderer renderer;
	  private final QuestionItemCallback callback;
	  private final boolean canAnswer;
	  private final Product product;
	  private final boolean canDelete;
	  @UiField
	  Button replyButton;
	  
	  public QuestionCell(QuestionItemCallback _callback, Product product, boolean canAnswer, boolean canDelete) {
	    super(BrowserEvents.CLICK, BrowserEvents.MOUSEOVER);
	 	renderer = GWT.create(Renderer.class);
	 	callback = _callback;
	 	this.canAnswer = canAnswer;
	 	this.product = product;
	 	this.canDelete = canDelete;
	  }

	  @Override
	  public void render(Context context, final Question item, final SafeHtmlBuilder sb) {
	    renderer.render(sb, item.getTitle(), item.getReply() != null ? item.getReply().getText() : "", 
	    		item.getReply() == null && canAnswer ? "display: block;" : "display: none;",
	    		item.getReply() != null ? "display: block;" : "display: none;",
    			canDelete ? "display: block;" : "display: none;");
	  }

	  @Override
	  public void onBrowserEvent(Context context, Element parent,
			  Question value, NativeEvent event, ValueUpdater<Question> valueUpdater) {
	    renderer.onBrowserEvent(this, event, parent, value);
	    
	    if ("click".equals(event.getType())) {
	        EventTarget eventTarget = event.getEventTarget();
	        if (!Element.is(eventTarget)) {
	            return;
	        }
	        if (parent.getElementsByTagName("input").getItem(0).isOrHasChild(Element.as(eventTarget))) {
	        	AnswerDialog dialog = new AnswerDialog(callback, product.getId(), value.getQid());
	    		dialog.center();
	    		dialog.show();
	        }
	        if (parent.getElementsByTagName("input").getItem(1).isOrHasChild(Element.as(eventTarget))) {
	        	callback.delete(product.getId(), value.getQid());
	        }
	    }
	  }
	 
	}