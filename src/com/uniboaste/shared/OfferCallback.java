package com.uniboaste.shared;

public interface OfferCallback {
	void offerDone(double amount);
}
