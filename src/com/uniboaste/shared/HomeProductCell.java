package com.uniboaste.shared;

import com.google.gwt.cell.client.AbstractCell;
import com.google.gwt.cell.client.ValueUpdater;
import com.google.gwt.core.client.GWT;
import com.google.gwt.dom.client.BrowserEvents;
import com.google.gwt.dom.client.Element;
import com.google.gwt.dom.client.NativeEvent;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.MouseOverEvent;
import com.google.gwt.safehtml.shared.SafeHtmlBuilder;
import com.google.gwt.uibinder.client.UiHandler;
import com.google.gwt.uibinder.client.UiRenderer;

public class HomeProductCell extends AbstractCell<Product> {
	
	static interface Renderer extends UiRenderer {
		void render(SafeHtmlBuilder sb, String title, String description, String author);
		void onBrowserEvent(HomeProductCell o, NativeEvent e, Element p, Product value);
	}

	  private final Renderer renderer;
	  private final ProductItemCallback callback;
	  
	  public HomeProductCell(ProductItemCallback _callback) {
	    super(BrowserEvents.CLICK, BrowserEvents.MOUSEOVER);
	 	renderer = GWT.create(Renderer.class);
	 	callback = _callback;
	  }

	  @Override
	  public void render(Context context, final Product item, final SafeHtmlBuilder sb) {
	    renderer.render(sb, item.getName(), item.getDescription(), item.getAuthor());
	  }

	  @Override
	  public void onBrowserEvent(Context context, Element parent,
			  Product value, NativeEvent event, ValueUpdater<Product> valueUpdater) {
	    renderer.onBrowserEvent(this, event, parent, value);
	  }

	  @UiHandler({"field1"})
	  void onMouseOver(MouseOverEvent event, Element parent, Product value) {
	  	
	  }

	  @UiHandler({"field1"})
	  void onReplyComment(ClickEvent event, Element parent, Product value) {
		  callback.clicked(value);
	  }
	}