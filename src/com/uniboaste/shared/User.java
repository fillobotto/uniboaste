package com.uniboaste.shared;

import java.io.Serializable;

import com.google.gwt.user.client.rpc.IsSerializable;

@SuppressWarnings("serial")
public class User implements IsSerializable, Serializable {

	public boolean isLoggedIn = false;
	
	private UserType type = UserType.Normal;
	
	public void setUserType(UserType type) {
		this.type = type;
	}
	
	public UserType getUserType() {
		return this.type;
	}
	
	
	private String username;
	
	public void setUsername(String username) {
		this.username = username;
	}
	
	public String getUsername() {
		return this.username;
	}
	
	private String password;
	
	public void setPassword(String password) {
		this.password = password;
	}
	
	public String getPassword() {
		return this.password;
	}
	
	private String name;
	
	public void setName(String name) {
		this.name = name;
	}
	
	public String getName() {
		return this.name;
	}
	
	private String surname;
	
	public void setSurname(String surname) {
		this.surname = surname;
	}
	
	public String getSurname() {
		return this.surname;
	}
	
	private String phone;
	
	public void setPhone(String phone) {
		this.phone = phone;
	}
	
	public String getPhone() {
		return this.phone;
	}
	
	private String email;
	
	public void setEmail(String email) {
		this.email = email;
	}
	
	public String getEmail() {
		return this.email;
	}
	
	private String cf;
	
	public void setCf(String cf) {
		this.cf = cf;
	}
	
	public String getCf() {
		return this.cf;
	}
	
	private String address;
	
	public void setAddress(String address) {
		this.address = address;
	}
	
	public String getAddress() {
		return this.address;
	}
	
	private String gender;
	
	public void setGender(String gender){
		this.gender = gender;
	}
	
	public String getGender(){
		return this.gender;
	}
	
	private String birth;
	
	public void setBirth(String birth) {
		this.birth = birth;
	}
	
	public String getBirth() {
		return this.birth;
	}
	
	private String birthplace;
	
	public void setBirthplace(String birthplace) {
		this.birthplace = birthplace;
	}
	
	public String getBirthplace() {
		return this.birthplace;
	}
	
}
