package com.uniboaste.shared;

import java.io.Serializable;

import com.google.gwt.user.client.rpc.IsSerializable;

public class Reply implements IsSerializable, Serializable {
	
	public Reply() {}
	
	public Reply(String text) {
		this.text = text;
	}
	


	private String text;

	public String getText() {
		return text;
	}

	public void setText(String text) {
		this.text = text;
	}	
}
