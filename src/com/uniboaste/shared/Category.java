package com.uniboaste.shared;

import java.io.Serializable;
import java.util.ArrayList;

import com.google.gwt.user.client.rpc.IsSerializable;

@SuppressWarnings("serial")
public class Category implements Serializable, IsSerializable {
	
	public Category() {
		this.children = new ArrayList<>();
	}
	
	public Category(String name) {
		this.children = new ArrayList<>();
		this.name = name;
	}
	
	private String name;

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}
	
	private ArrayList<Category> children;
	
	public void addChild(Category category) {
		this.children.add(category);
	}
	
	public ArrayList<Category> getChildren() {
		return this.children;
	}
}
