package com.uniboaste.shared;

public interface ProfilePageCallback {
	void openProduct(Product product);
}
