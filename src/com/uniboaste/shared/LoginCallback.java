package com.uniboaste.shared;

public interface LoginCallback {
	void loggedIn(User user);
}
