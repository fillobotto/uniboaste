package com.uniboaste.shared;

public interface ProductItemCallback {
	void clicked(Product product);
}
