package com.uniboaste.shared;

public interface QuestionItemCallback {
	void answer(String pid, String qid, String reply);
	
	void delete(String pid, String qid);
}
