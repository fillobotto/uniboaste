package com.uniboaste.shared;

import java.io.Serializable;

import com.google.gwt.user.client.rpc.IsSerializable;

public class Question implements IsSerializable, Serializable {
	
	public Question() {}
	
	public Question(String qid, String title) {
		this.qid = qid;
		this.title = title;
	}
	
	private String qid;
	
	public String getQid() {
		return qid;
	}

	public void setQid(String qid) {
		this.qid = qid;
	}

	private String title;
	
	private Reply reply;

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public Reply getReply() {
		return reply;
	}

	public void setReply(Reply reply) {
		this.reply = reply;
	}
	
}
