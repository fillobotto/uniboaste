package com.uniboaste.shared;

import java.io.Serializable;

import com.google.gwt.user.client.rpc.IsSerializable;

public class Bid implements Serializable, IsSerializable {

	public Bid() {

	}
	
	public Bid(String name, String author, double amount) {
		this.amount = amount;
		this.author = author;
		this.productName = name;
	}
	
	public String getAuthor() {
		return author;
	}

	public void setAuthor(String author) {
		this.author = author;
	}

	public double getAmount() {
		return amount;
	}

	public void setAmount(double amount) {
		this.amount = amount;
	}

	private String author;
	
	private double amount;
	
	private boolean isHigest = false;
	
	public boolean isHigest() {
		return isHigest;
	}

	public void setHigest(boolean isHigest) {
		this.isHigest = isHigest;
	}

	public boolean isClosed() {
		return isClosed;
	}

	public void setClosed(boolean isClosed) {
		this.isClosed = isClosed;
	}

	private boolean isClosed;
	
	private String productName;

	public String getProductName() {
		return productName;
	}

	public void setProductName(String productName) {
		this.productName = productName;
	}
}
