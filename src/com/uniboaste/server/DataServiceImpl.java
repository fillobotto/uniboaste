package com.uniboaste.server;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import org.mapdb.BTreeMap;
import org.mapdb.DB;

import com.google.gwt.user.client.ui.TreeItem;
import com.uniboaste.client.services.DataService;
import com.uniboaste.shared.Bid;
import com.uniboaste.shared.Category;
import com.uniboaste.shared.Product;
import com.uniboaste.shared.Question;
import com.uniboaste.shared.Reply;
import com.uniboaste.shared.Utils;

/**
 * The server-side implementation of the RPC service.
 */
@SuppressWarnings("serial")
public class DataServiceImpl extends AdvancedServlet implements DataService {

	/***
	 * Ottiene l'albero delle categorie
	 */
	@Override
	public Category getCategoryTree() {
		DB db = getDB();
		BTreeMap<Integer, Category> map = db.getTreeMap("categories");
		Category c = map.values().iterator().next();
		return c;
	}

	/***
	 * Aggiunge un nuovo prodotto
	 */
	@Override
	public void addProduct(Product product) {
		if(getUser() == null) return;
		DB db = getDB();
		BTreeMap<Integer, Product> map = db.getTreeMap("products");
		product.setAuthor(getUser().getUsername());
		product.setOpen(true);
		product.setId(Utils.getTimestampHash());
		map.put(map.size() + 1, product);
		db.commit();
	}

	
	/***
	 * Ottiene le sottocategorie di una categoria
	 * @param name
	 * @return
	 */
	private ArrayList<Category> getDescendants(String name) {
		ArrayList<Category> cats = new ArrayList<>();

		for (Category c : getCategoryTree().getChildren()) {
			boolean toAdd = name.equals("Categories");

			if (c.getName().equals(name))
				toAdd = true;
			if (toAdd)
				cats.add(c);
			setC1(cats, c, toAdd, name);
		}

		return cats;
	}

	void setC1(ArrayList<Category> cats, Category category, boolean toAdd, String name) {
		if (category.getName().equals(name))
			toAdd = true;
		if (toAdd)
			cats.add(category);

		for (Category c : category.getChildren()) {
			setC1(cats, c, toAdd, name);
		}
	}

	/***
	 * Ottiene i prodotti appartenenti ad una categoria
	 */
	@Override
	public ArrayList<Product> getProductsByCategory(String name) {
		closeExpiredAuctions();
		ArrayList<Product> prs = new ArrayList<>();
		ArrayList<Category> cats = getDescendants(name);

		DB db = getDB();
		BTreeMap<Integer, Product> map = db.getTreeMap("products");

		for (Product p : map.values()) {
			boolean toAdd = false;
			for (Category c : cats)
				if (c.getName().equals(p.getCategory()) && p.isOpen())
					toAdd = true;
			if (toAdd)
				prs.add(p);
		}

		return prs;
	}

	/***
	 * Imposta come chiuse tutte le inserzioni scadute
	 */
	private void closeExpiredAuctions() {
		DB db = getDB();
		BTreeMap<Integer, Product> map = db.getTreeMap("products");

		for (Product p : map.values()) {
			long d = p.getExpirationDate();
			Calendar calendar = Calendar.getInstance();
			calendar.setTimeInMillis(d);
			if (calendar.getTime().before(new Date()))
				p.setOpen(false);
		}

		db.commit();
	}

	/***
	 * Ottiene un prodotto dato il suo id
	 * @param pid
	 * @return
	 */
	private Product getProductById(String pid) {
		DB db = getDB();
		BTreeMap<Integer, Product> map = db.getTreeMap("products");

		for (Product p : map.values()) {
			if (p.getId().equals(pid))
				return p;
		}

		return null;
	}

	/***
	 * Aggiunge una domanda ad un oggetto
	 */
	@Override
	public void addQuestion(String pid, String title) {
		DB db = getDB();
		BTreeMap<Integer, Product> map = db.getTreeMap("products");

		for (Entry<Integer, Product> p : map.entrySet()) {
			if (p.getValue().getId().equals(pid)) {
				ArrayList<Question> questions = p.getValue().getQuestions();
				questions.add(new Question(Utils.getTimestampHash(), title));
				p.getValue().setQuestions(questions);
				map.put(p.getKey(), p.getValue());
				db.commit();
			}
		}
	}

	/***
	 * Aggiunge u'offerta ad un offetto
	 */
	@Override
	public void addBid(String pid, double amount) {
		if (getUser() == null)
			return;

		Product origP = getProductById(pid);

		ArrayList<Bid> bids = origP.getBids();
		if (bids.size() > 0) {
			if (bids.get(0).getAmount() >= amount)
				return;
			if (bids.get(0).getAuthor().equals(getUser().getUsername()))
				return;
		}

		if (origP != null && origP.getPrize() >= amount)
			return;
		if (origP != null && origP.getAuthor().equals(getUser().getUsername()))
			return;

		DB db = getDB();
		BTreeMap<Integer, Product> map = db.getTreeMap("products");

		for (Entry<Integer, Product> p : map.entrySet()) {
			if (p.getValue().getId().equals(pid)) {
				bids.add(new Bid(origP.getName(), getUser().getUsername(), amount));
				p.getValue().setBids(bids);
				map.put(p.getKey(), p.getValue());
				db.commit();
			}
		}
	}

	/***
	 * Ottiene le offerte per un oggetto
	 */
	@Override
	public ArrayList<Bid> getBids(String pid) {
		ArrayList<Bid> bids = new ArrayList<>();
		DB db = getDB();
		BTreeMap<Integer, Product> map = db.getTreeMap("products");

		for (Product p : map.values()) {
			if (p.getId().equals(pid)) {
				bids = p.getBids();
			}
		}

		Collections.sort(bids, new CustomComparator());
		Collections.reverse(bids);
		return bids;
	}

	/***
	 * Ottiene le domande per un oggetto
	 */
	@Override
	public ArrayList<Question> getQuestions(String pid) {
		ArrayList<Question> question = new ArrayList<>();
		DB db = getDB();
		BTreeMap<Integer, Product> map = db.getTreeMap("products");

		for (Product p : map.values()) {
			if (p.getId().equals(pid)) {
				question = p.getQuestions();
			}
		}

		return question;
	}

	public class CustomComparator implements Comparator<Bid> {
		@Override
		public int compare(Bid o1, Bid o2) {
			if (o1.getAmount() < o2.getAmount())
				return -1;
			if (o1.getAmount() > o2.getAmount())
				return 1;
			return 0;
		}
	}

	public class CustomComparator2 implements Comparator<Product> {
		@Override
		public int compare(Product o1, Product o2) {
			if (o1.getExpirationDate() < o2.getExpirationDate())
				return -1;
			if (o1.getExpirationDate() > o2.getExpirationDate())
				return 1;
			return 0;
		}
	}

	/***
	 * Rimuove un oggetto dato il suo id
	 */
	@Override
	public void deleteProduct(String pid) {
		DB db = getDB();
		BTreeMap<Integer, Product> map = db.getTreeMap("products");

		for (Iterator<Map.Entry<Integer, Product>> it = map.entrySet().iterator(); it.hasNext();) {
			Map.Entry<Integer, Product> entry = it.next();
			if (entry.getValue().getId().equals(pid)) {
				it.remove();
			}
		}

		db.commit();
	}

	/***
	 * Ottiene gli oggetti messi in vendita da un utente
	 */
	@Override
	public ArrayList<Product> getOwnProducts() {
		closeExpiredAuctions();
		ArrayList<Product> prs = new ArrayList<>();

		DB db = getDB();
		BTreeMap<Integer, Product> map = db.getTreeMap("products");

		for (Product p : map.values()) {
			if (getUser() != null && getUser().getUsername().equals(p.getAuthor()))
				prs.add(p);
		}

		return prs;
	}

	/***
	 * Ottiene gli oggetti con inserzione non scaduta
	 */
	@Override
	public ArrayList<Product> getOpenProducts() {
		closeExpiredAuctions();
		ArrayList<Product> prs = new ArrayList<>();

		DB db = getDB();
		BTreeMap<Integer, Product> map = db.getTreeMap("products");

		for (Product p : map.values()) {
			if (p.isOpen())
				prs.add(p);
		}

		Collections.sort(prs, new CustomComparator2());
		return prs;
	}

	/***
	 * Ottiene le offerte effettuate da un utente
	 */
	@Override
	public ArrayList<Bid> getOwnBids() {
		closeExpiredAuctions();
		ArrayList<Bid> prs = new ArrayList<>();

		DB db = getDB();
		BTreeMap<Integer, Product> map = db.getTreeMap("products");

		for (Product p : map.values()) {
			Bid tempBid = null;

			for (Bid b : p.getBids()) {
				if (getUser() != null && getUser().getUsername().equals(b.getAuthor()))
					tempBid = b;
			}

			if (tempBid != null) {
				Collections.sort(p.getBids(), new CustomComparator());
				Collections.reverse(p.getBids());

				if (p.getBids().get(0).getAmount() <= tempBid.getAmount())
					tempBid.setHigest(true);

				tempBid.setClosed(!p.isOpen());
				prs.add(tempBid);
			}
		}

		return prs;
	}

	/***
	 * Aggiunge una risposta ad una domanda
	 */
	@Override
	public void answerQuestion(String pid, String qid, String reply) {
		DB db = getDB();
		BTreeMap<Integer, Product> map = db.getTreeMap("products");

		for (Entry<Integer, Product> p : map.entrySet()) {
			if (p.getValue().getId().equals(pid)) {
				ArrayList<Question> question = new ArrayList<>();
				question = p.getValue().getQuestions();

				for (int i = 0; i < question.size(); i++)
					if (question.get(i).getQid().equals(qid))
						question.get(i).setReply(new Reply(reply));

				p.getValue().setQuestions(question);
				map.put(p.getKey(), p.getValue());
				db.commit();
			}
		}
	}

	/***
	 * Elimina una domanda
	 */
	@Override
	public void deleteQuestion(String pid, String qid) {
		DB db = getDB();
		BTreeMap<Integer, Product> map = db.getTreeMap("products");

		for (Entry<Integer, Product> p : map.entrySet()) {
			if (p.getValue().getId().equals(pid)) {
				ArrayList<Question> question = new ArrayList<>();
				question = p.getValue().getQuestions();

				for (int i = 0; i < question.size(); i++)
					if (question.get(i).getQid().equals(qid))
						question.remove(i);

				p.getValue().setQuestions(question);
				map.put(p.getKey(), p.getValue());
				db.commit();
			}
		}
	}

	@Override
	public void deleteLastBid(String pid) {
		DB db = getDB();
		BTreeMap<Integer, Product> map = db.getTreeMap("products");

		for (Entry<Integer, Product> p : map.entrySet()) {
			if (p.getValue().getId().equals(pid)) {
				ArrayList<Bid> bids = new ArrayList<>();
				bids = p.getValue().getBids();

				if (bids.size() == 0)
					return;
				Collections.sort(bids, new CustomComparator());
				Collections.reverse(bids);

				bids.remove(0);
				p.getValue().setBids(bids);
				map.put(p.getKey(), p.getValue());
				db.commit();
			}
		}
	}
	
	@Override
	public void renameCategory(String query, String to) {
		DB db = getDB();
		BTreeMap<Integer, Category> map = db.getTreeMap("categories");
		Category c = map.values().iterator().next();
		

		for(Category c0 : c.getChildren()) {
			setC1(c0, query, to);
		}
		
		map.put(0, c);
		
		BTreeMap<Integer, Product> map2 = db.getTreeMap("products");
		
		for (Entry<Integer, Product> p : map2.entrySet()) {
			if (p.getValue().getCategory().equals(query)) {
				p.getValue().setCategory(to);
				map2.put(p.getKey(), p.getValue());
			}
		}
		
		db.commit();
	}
	
	void setC1(Category category, String query, String to) {
		if(category.getName().equals(query))
			category.setName(to);
		
		for(Category c : category.getChildren()) {
			setC(c, query, to);
		}
	}
	
	void setC(Category category, String query, String to) {
		if(category.getName().equals(query))
			category.setName(to);
		
		for(Category c : category.getChildren()) {
			setC(c, query, to);
		}
	}
	
	@Override
	public void addCategory(String query, String to) {
		DB db = getDB();
		BTreeMap<Integer, Category> map = db.getTreeMap("categories");
		Category c = map.values().iterator().next();
		
		if(c.getName().equals(query)) {
			c.addChild(new Category(to));
		} else {
			for(Category c0 : c.getChildren()) {
				setC2(c0, query, to);
			}
		}

		
		
		map.put(0, c);
		
		db.commit();
	}
	
	void setC2(Category category, String query, String to) {
		if(category.getName().equals(query))
			category.addChild(new Category(to));
		
		for(Category c : category.getChildren()) {
			setC0(c, query, to);
		}
	}
	
	void setC0(Category category, String query, String to) {
		if(category.getName().equals(query))
			category.addChild(new Category(to));
		
		for(Category c : category.getChildren()) {
			setC0(c, query, to);
		}
	}
}
