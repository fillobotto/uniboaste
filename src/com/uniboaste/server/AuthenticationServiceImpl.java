package com.uniboaste.server;

import com.uniboaste.client.services.AuthenticationService;
import com.uniboaste.shared.User;
import org.mapdb.BTreeMap;
import org.mapdb.DB;

/**
 * The server-side implementation of the RPC service.
 */
public class AuthenticationServiceImpl extends AdvancedServlet implements AuthenticationService {
	private static final long serialVersionUID = -724775795619716589L;

	/***
	 * Esegue il logout della sessione utente
	 */
	@Override
	public void logout() {
		setUser(null);
	}

	/***
	 * Ottiene l'utente correntemente autenticato
	 */
	@Override
	public User isAuthenticated() {
		return getUser();
	}
	
	/***
	 * Effettua il login
	 */
	@Override
    public User login(String username, String password)
    {
		User user = null;
		
		DB db = getDB();
		BTreeMap<Integer, User> map = db.getTreeMap("users");
		
		
		for(User u : map.values()) {
			if(u.getUsername().equals(username) && u.getPassword().equals(password)) {
				setUser(u);
				user = u;
			}
		}
		
        return user;
    }
	
	/***
	 * Effettua la registrazione
	 */
	@Override
	public Boolean register(String username, String name, String surname, String mobile, String password, String email, String address, String cf, String gender, String birth, String birthplace) {
		User user = new User();
		user.setUsername(username);
		user.setPassword(password);
		user.setName(name);
		user.setSurname(surname);
		user.setPhone(mobile);
		user.setEmail(email);
		user.setCf(cf);
		user.setAddress(address);
		user.setGender(gender);
		user.setBirth(birth);
		user.setBirthplace(birthplace);
		
		DB db = getDB();
		BTreeMap<Integer, User> map = db.getTreeMap("users");
		
		for(User u : map.values()) {
			if(u.getUsername().equals(username)) {
				return false;
			}
		}
		
		map.put(map.size() + 1, user);
		db.commit();

		return true;
	}
	
}
