package com.uniboaste.server;

import java.io.File;

import javax.servlet.ServletContext;

import org.mapdb.BTreeMap;
import org.mapdb.DB;
import org.mapdb.DBMaker;

import com.google.gwt.user.server.rpc.RemoteServiceServlet;
import com.uniboaste.shared.Category;
import com.uniboaste.shared.User;
import com.uniboaste.shared.UserType;

public class AdvancedServlet extends RemoteServiceServlet {
	
	private static final long serialVersionUID = -361100915651387491L;

	public User getUser() {
		return (User) getThreadLocalRequest().getSession().getAttribute("CurrentUser");
	}
	
	public void setUser(User user) {
		getThreadLocalRequest().getSession().setAttribute("CurrentUser", user);
	}
	
	/*
	 * We store the DB in the servlet context
	 * to implement a poor man's singleton
	 */
	public DB getDB() {
		ServletContext context = this.getServletContext();
		synchronized (context) {
			DB db = (DB)context.getAttribute("DB");
			if(db == null) {
				db = DBMaker.newFileDB(new File("db")).closeOnJvmShutdown().make();
				context.setAttribute("DB", db);
				
				// Database entry initialization
				
				BTreeMap<Integer, User> map = db.getTreeMap("users");
				
				User user = new User();
				user.setUserType(UserType.Administrator);
				user.setUsername("admin");
				user.setName("Admin");
				user.setSurname("Admin");
				user.setAddress("Admin");
				user.setBirth("Admin");
				user.setBirthplace("Admin");
				user.setPassword("admin");
				user.setEmail("admin");
				user.setCf("admin");
				user.setPhone("admin");
				user.setGender("Admin");
				
				Category root = new Category("root");
				root.addChild(new Category("Casa"));
				root.addChild(new Category("Abbigliamento"));
				root.addChild(new Category("Elettronica"));
				root.addChild(new Category("Giardinaggio"));
				Category c = new Category("Sport");
				Category b = new Category("Scarpe da calcio");
				b.addChild(new Category("Con tacchetti"));
				b.addChild(new Category("Senza tacchetti"));
				c.addChild(b);
				c.addChild(new Category("Basket"));
				root.addChild(c);
				
				BTreeMap<Integer, Category> categories = db.getTreeMap("categories");
				
				if(categories.size() == 0)
					categories.put(0, root);
				
				map.put(0, user);
				db.commit();
			}
			return db;
		}
	}
}